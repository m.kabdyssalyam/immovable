package kz.immovable.web.rest.errors;

public class NotAppropriateRoleChoosenException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public NotAppropriateRoleChoosenException() {
        super(ErrorConstants.NOT_APPROPRIATE_ROLE_CHOOSEN, "Email is already in use!", "userManagement", "emailexists");
    }
}
