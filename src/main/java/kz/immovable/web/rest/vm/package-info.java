/**
 * View Models used by Spring MVC REST controllers.
 */
package kz.immovable.web.rest.vm;
