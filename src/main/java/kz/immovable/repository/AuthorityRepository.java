package kz.immovable.repository;

import kz.immovable.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
    @Override
    List<Authority> findAllById(Iterable<String> iterable);
}
